package net.fabcc;

import java.util.ArrayList;
import java.util.List;

public class Polynomial {

  /**
   * Contains all the monomial
   */
  private List<Monomial> monomials;

  /**
   *
   */
  public Polynomial() { this.monomials = new ArrayList<>(); }

  public String print() {
    // Print the polynomial
    String result = "";
    for (int i = 0; i < this.monomials.size(); i++) {
      Monomial monomial = this.monomials.get(i);
      switch (monomial.getExponent()) {
      case 0:
        // Case of C
        result += monomial.getCoefficient();
        break;
      case 1:
        // Case of C.x
        result += monomial.getCoefficient() + ".x";
        break;
      default:
        // Case of C.x^E
        result += monomial.getCoefficient() + ".x^" + monomial.getExponent();
        break;
      }
      if (i != this.monomials.size() - 1) {
        // if not last monomial, add a + to concat
        result += "+";
      }
    }
    return result;
  }

  public void setPolynomial(String poly) {
    // Polynomial is form of (C.x^E)[...]
    this.monomials.clear();
    String[] stringMonomials = poly.split("\\+");
    for (String stringMonomial : stringMonomials) {
      // Form of a monomial is C(.x(^E))
      // as we can see, X might not be there, and also E
      // but when E is here, X is always here since we cannot have exponent
      // without the variable X
      String[] component = stringMonomial.split("[.^]");
      switch (component.length) {
      case 3:
        // Case of C.x^E
        this.monomials.add(new Monomial(Integer.parseInt(component[0]),
                                        Integer.parseInt(component[2])));
        break;
      case 2:
        // Case of C.x
        this.monomials.add(new Monomial(Integer.parseInt(component[0]), 1));
        break;
      case 1:
        // Case of C
        this.monomials.add(new Monomial(Integer.parseInt(component[0]), 0));
        break;
      default:
        // Case ????
        break;
      }
    }
  }

  /**
   * Evaluate the polynomial value for a given X input
   * @param x the input
   * @return the value of the polynomial at that input
   */
  public float compute(float x) {
    float sum = 0;
    for (Monomial monomial : this.monomials) {
      sum += monomial.evaluate(x);
    }
    return sum;
  }
}
