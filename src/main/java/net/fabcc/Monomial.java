package net.fabcc;

public class Monomial {

  private int coefficient;
  private int exponent;

  /**
   *
   * @param coefficient
   * @param exponent
   */
  public Monomial(int coefficient, int exponent) {
    this.coefficient = coefficient;
    this.exponent = exponent;
  }

  /**
   *
   * Apply coefficient and exponent to X
   *
   * @return C*X^E
   */
  public float evaluate(float x) {
    return (float)(Float.valueOf(this.coefficient) *
                   Math.pow(x, this.exponent));
  }

  /**
   * @return the coefficient
   */
  public int getCoefficient() { return coefficient; }

  /**
   * @return the exponent
   */
  public int getExponent() { return exponent; }
}
