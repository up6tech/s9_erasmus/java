package net.fabcc;

/**
 * Hello world!
 *
 */
public class App {
  public static void main(String[] args) {
    String polynomial = "5.x^2+3.x+2";
    Polynomial p = new Polynomial();
    p.setPolynomial(polynomial);
    System.out.println(p.print());
  }
}
